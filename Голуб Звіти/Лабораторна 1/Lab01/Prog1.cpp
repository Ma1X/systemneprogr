#include <windows.h>
#include <iostream>
#include <conio.h>
#include <time.h>


int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	HANDLE hmutex = CreateMutex(NULL, true, "Mutex");
	HANDLE hFile;
	HANDLE hFileMapping;
	int* buf = NULL;
	LPCSTR fname = "data.dat";
	hFile = CreateFile(fname, GENERIC_READ | GENERIC_WRITE | GENERIC_EXECUTE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != NULL)
	{
		printf("File's HANDLE is OK!\n");
	}
	else
	{
		printf("Could not open %s file, error %d\n", fname, GetLastError());
		return 0;
	}

	DWORD dwNumOfBytes;
	srand(time(0));
	for (int i = 0; i < 25; i++)
	{
		int num = rand() % 91 + 10;
		WriteFile(hFile, &num, sizeof(int), &dwNumOfBytes, NULL);
	}

	hFileMapping = CreateFileMapping(hFile, NULL, PAGE_EXECUTE_READWRITE, NULL, NULL, "data");
	if (hFileMapping == nullptr)
	{
		printf("fileMappingCreate - CreateFileMapping failed, fname = %s\n", fname);
		CloseHandle(hFile);
		return 0;
	}
	else {
		printf("fileMappingCreate - CreateFileMapping approved, fname = %s\n", fname);
	}

	buf = (int*)MapViewOfFile(hFileMapping, FILE_MAP_ALL_ACCESS, NULL, NULL, NULL);
	if (buf == NULL) {
		printf("fileMappingCreate - MapViewOfFile failed, fname = %s\n", fname);
		CloseHandle(hFileMapping);
		CloseHandle(hFile);
		return 0;
	}
	else {
		printf("fileMappingCreate - MapViewOfFile approved, fname = %s\n", fname);
	}

	printf("�������� �����:\n");
	for (int i = 0; i < 25; i++)
	{
		printf("%d ", buf[i]);
	}
	printf("\n");

	printf("��������� ����-��� ������...\n");
	ReleaseMutex(hmutex);
	getchar();
	UnmapViewOfFile(buf);
	CloseHandle(hFileMapping);
	CloseHandle(hFile);
	return 0;

}
