﻿namespace Lab04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxAllUserPrograms = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBoxCurrentUserPrograms = new System.Windows.Forms.RichTextBox();
            this.richTextBoxScheduler = new System.Windows.Forms.RichTextBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonAddToAutoStart = new System.Windows.Forms.Button();
            this.textBoxProgramToAutorun = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.richTextBoxCurrentUserTasks = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonAddToAssociation = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxSubKey = new System.Windows.Forms.TextBox();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBoxAllUserPrograms
            // 
            this.richTextBoxAllUserPrograms.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxAllUserPrograms.Location = new System.Drawing.Point(14, 29);
            this.richTextBoxAllUserPrograms.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBoxAllUserPrograms.Name = "richTextBoxAllUserPrograms";
            this.richTextBoxAllUserPrograms.Size = new System.Drawing.Size(464, 210);
            this.richTextBoxAllUserPrograms.TabIndex = 1;
            this.richTextBoxAllUserPrograms.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Всі програми:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(492, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Поточні програми:";
            // 
            // richTextBoxCurrentUserPrograms
            // 
            this.richTextBoxCurrentUserPrograms.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxCurrentUserPrograms.Location = new System.Drawing.Point(495, 29);
            this.richTextBoxCurrentUserPrograms.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBoxCurrentUserPrograms.Name = "richTextBoxCurrentUserPrograms";
            this.richTextBoxCurrentUserPrograms.Size = new System.Drawing.Size(484, 210);
            this.richTextBoxCurrentUserPrograms.TabIndex = 3;
            this.richTextBoxCurrentUserPrograms.Text = "";
            // 
            // richTextBoxScheduler
            // 
            this.richTextBoxScheduler.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxScheduler.Location = new System.Drawing.Point(17, 275);
            this.richTextBoxScheduler.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBoxScheduler.Name = "richTextBoxScheduler";
            this.richTextBoxScheduler.Size = new System.Drawing.Size(461, 247);
            this.richTextBoxScheduler.TabIndex = 6;
            this.richTextBoxScheduler.Text = "";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.Location = new System.Drawing.Point(802, 607);
            this.buttonRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(177, 31);
            this.buttonRefresh.TabIndex = 7;
            this.buttonRefresh.Text = "Оновити";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonAddToAutoStart
            // 
            this.buttonAddToAutoStart.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddToAutoStart.Location = new System.Drawing.Point(241, 560);
            this.buttonAddToAutoStart.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddToAutoStart.Name = "buttonAddToAutoStart";
            this.buttonAddToAutoStart.Size = new System.Drawing.Size(177, 27);
            this.buttonAddToAutoStart.TabIndex = 8;
            this.buttonAddToAutoStart.Text = "Додати";
            this.buttonAddToAutoStart.UseVisualStyleBackColor = true;
            this.buttonAddToAutoStart.Click += new System.EventHandler(this.buttonAddToAutoStart_Click);
            // 
            // textBoxProgramToAutorun
            // 
            this.textBoxProgramToAutorun.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxProgramToAutorun.Location = new System.Drawing.Point(21, 560);
            this.textBoxProgramToAutorun.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxProgramToAutorun.Name = "textBoxProgramToAutorun";
            this.textBoxProgramToAutorun.Size = new System.Drawing.Size(190, 27);
            this.textBoxProgramToAutorun.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 255);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(197, 18);
            this.label5.TabIndex = 10;
            this.label5.Text = "Завдання за розкладом:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // richTextBoxCurrentUserTasks
            // 
            this.richTextBoxCurrentUserTasks.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxCurrentUserTasks.Location = new System.Drawing.Point(495, 275);
            this.richTextBoxCurrentUserTasks.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBoxCurrentUserTasks.Name = "richTextBoxCurrentUserTasks";
            this.richTextBoxCurrentUserTasks.Size = new System.Drawing.Size(484, 247);
            this.richTextBoxCurrentUserTasks.TabIndex = 12;
            this.richTextBoxCurrentUserTasks.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(493, 255);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Поточні завдання:";
            // 
            // buttonAddToAssociation
            // 
            this.buttonAddToAssociation.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddToAssociation.Location = new System.Drawing.Point(799, 558);
            this.buttonAddToAssociation.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddToAssociation.Name = "buttonAddToAssociation";
            this.buttonAddToAssociation.Size = new System.Drawing.Size(180, 31);
            this.buttonAddToAssociation.TabIndex = 13;
            this.buttonAddToAssociation.Text = "Додати .ttt до асоціації";
            this.buttonAddToAssociation.UseVisualStyleBackColor = true;
            this.buttonAddToAssociation.Click += new System.EventHandler(this.buttonAddToAssociation_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 589);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Розділ:";
            // 
            // textBoxSubKey
            // 
            this.textBoxSubKey.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSubKey.Location = new System.Drawing.Point(21, 607);
            this.textBoxSubKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSubKey.Name = "textBoxSubKey";
            this.textBoxSubKey.Size = new System.Drawing.Size(190, 27);
            this.textBoxSubKey.TabIndex = 15;
            // 
            // buttonCopy
            // 
            this.buttonCopy.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCopy.Location = new System.Drawing.Point(241, 609);
            this.buttonCopy.Margin = new System.Windows.Forms.Padding(2);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(177, 25);
            this.buttonCopy.TabIndex = 16;
            this.buttonCopy.Text = "Копіювати";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(14, 542);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(189, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Програма для автостарту:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 680);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.buttonCopy);
            this.Controls.Add(this.textBoxSubKey);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.buttonAddToAssociation);
            this.Controls.Add(this.richTextBoxCurrentUserTasks);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxProgramToAutorun);
            this.Controls.Add(this.buttonAddToAutoStart);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.richTextBoxScheduler);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBoxCurrentUserPrograms);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextBoxAllUserPrograms);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lab04";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox richTextBoxAllUserPrograms;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBoxCurrentUserPrograms;
        private System.Windows.Forms.RichTextBox richTextBoxScheduler;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonAddToAutoStart;
        private System.Windows.Forms.TextBox textBoxProgramToAutorun;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox richTextBoxCurrentUserTasks;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonAddToAssociation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxSubKey;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.Label label8;
    }
}

