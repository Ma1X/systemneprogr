﻿namespace Lab03
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.driveInfo = new System.Windows.Forms.ListBox();
            this.currentDirectoryInfo = new System.Windows.Forms.ListBox();
            this.systemDirectoryInfo = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.fileSystemWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).BeginInit();
            this.SuspendLayout();
            // 
            // driveInfo
            // 
            this.driveInfo.FormattingEnabled = true;
            this.driveInfo.Location = new System.Drawing.Point(539, 20);
            this.driveInfo.Name = "driveInfo";
            this.driveInfo.Size = new System.Drawing.Size(335, 524);
            this.driveInfo.TabIndex = 0;
            // 
            // currentDirectoryInfo
            // 
            this.currentDirectoryInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.currentDirectoryInfo.FormattingEnabled = true;
            this.currentDirectoryInfo.Location = new System.Drawing.Point(12, 293);
            this.currentDirectoryInfo.Name = "currentDirectoryInfo";
            this.currentDirectoryInfo.Size = new System.Drawing.Size(491, 251);
            this.currentDirectoryInfo.TabIndex = 1;
            // 
            // systemDirectoryInfo
            // 
            this.systemDirectoryInfo.FormattingEnabled = true;
            this.systemDirectoryInfo.Location = new System.Drawing.Point(12, 20);
            this.systemDirectoryInfo.Name = "systemDirectoryInfo";
            this.systemDirectoryInfo.Size = new System.Drawing.Size(491, 251);
            this.systemDirectoryInfo.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(376, 569);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(193, 75);
            this.button1.TabIndex = 3;
            this.button1.Text = "Оновити";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // fileSystemWatcher
            // 
            this.fileSystemWatcher.EnableRaisingEvents = true;
            this.fileSystemWatcher.SynchronizingObject = this;
            this.fileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(this.OnChanged);
            this.fileSystemWatcher.Renamed += new System.IO.RenamedEventHandler(this.OnRenamed);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 670);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.systemDirectoryInfo);
            this.Controls.Add(this.currentDirectoryInfo);
            this.Controls.Add(this.driveInfo);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DiskInfo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox driveInfo;
        private System.Windows.Forms.ListBox currentDirectoryInfo;
        private System.Windows.Forms.ListBox systemDirectoryInfo;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        public System.IO.FileSystemWatcher fileSystemWatcher;
    }
}

