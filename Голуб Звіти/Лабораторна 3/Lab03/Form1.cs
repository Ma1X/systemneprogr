﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;

namespace Lab03
{
    public partial class Form1 : Form
    {
        protected readonly PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        public Form1()
        {
            InitializeComponent();
        }

        public void ShowDisksInfo()
        {
            int i = 0;
            int j = 0;
            currentDirectoryInfo.Items.Insert(j++, ($"Поточна директорія: { Directory.GetCurrentDirectory() }"));
            currentDirectoryInfo.Items.Insert(j++, ($"Ім'я користувача: {SystemInformation.UserName}"));
            currentDirectoryInfo.Items.Insert(j++, ($"Назва машини користувача: {Environment.MachineName}"));

            int m = 0;
            string dirName = Environment.SystemDirectory;
            string dirName1 = Path.GetTempPath();
            string dirName2 = Directory.GetCurrentDirectory();
            DirectoryInfo dirInfo = new DirectoryInfo(dirName);
            DirectoryInfo dirInfo1 = new DirectoryInfo(dirName1);
            DirectoryInfo dirInfo2 = new DirectoryInfo(dirName2);
            systemDirectoryInfo.Items.Insert(m++, $"Системна директорія: {Environment.CurrentDirectory = dirName}");
            systemDirectoryInfo.Items.Insert(m++, $"Назва каталогу: { dirInfo.Name }");
            systemDirectoryInfo.Items.Insert(m++, $"Повна назва каталогу: { dirInfo.FullName }");
            systemDirectoryInfo.Items.Insert(m++, $"Дата створення каталогу: { dirInfo.CreationTime }");
            systemDirectoryInfo.Items.Insert(m++, $"Шлях до каталогу: { dirInfo.Root }");
            systemDirectoryInfo.Items.Insert(m++, " ");
            systemDirectoryInfo.Items.Insert(m++, $"Тимчасова директорія: {Environment.CurrentDirectory = dirName1}");
            systemDirectoryInfo.Items.Insert(m++, $"Назва каталогу: { dirInfo1.Name }");
            systemDirectoryInfo.Items.Insert(m++, $"Повна назва каталогу: { dirInfo1.FullName }");
            systemDirectoryInfo.Items.Insert(m++, $"Дата створення каталогу: { dirInfo1.CreationTime }");
            systemDirectoryInfo.Items.Insert(m++, $"Шлях до каталогу: { dirInfo1.Root }");
            systemDirectoryInfo.Items.Insert(m++, " ");
            systemDirectoryInfo.Items.Insert(m++, $"Поточна директорія: {Environment.CurrentDirectory = dirName2}");
            systemDirectoryInfo.Items.Insert(m++, $"Назва каталогу: { dirInfo2.Name }");
            systemDirectoryInfo.Items.Insert(m++, $"Повна назва каталогу: { dirInfo2.FullName }");
            systemDirectoryInfo.Items.Insert(m++, $"Дата створення каталогу: { dirInfo2.CreationTime }");
            systemDirectoryInfo.Items.Insert(m++, $"Шлях до каталогу: { dirInfo2.Root }");

            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach (DriveInfo d in allDrives)
            {
                driveInfo.Items.Insert(i++, $"Drive { d.Name } File type: { d.DriveType }");
                if (d.IsReady == true)
                {
                    driveInfo.Items.Insert(i++, $"Мітка тома: {d.VolumeLabel}");
                    driveInfo.Items.Insert(i++, $"Файлова система: {d.DriveFormat}");
                    driveInfo.Items.Insert(i++, $"Корнева директорія: {d.RootDirectory}");
                    driveInfo.Items.Insert(i++, $"Доступне місце для поточного юзера: {d.AvailableFreeSpace / 1024 / 1024 / 1024} Гігабайт");
                    driveInfo.Items.Insert(i++, $"Загальне доступне місце: {d.TotalFreeSpace / 1024 / 1024 / 1024} Гігабайт");
                    driveInfo.Items.Insert(i++, $"Загальний об'єм диску: {d.TotalSize / 1024 / 1024 / 1024} Гігабайт");
                    driveInfo.Items.Insert(i++, $"Системна пам'ять: { ramCounter.NextValue() } Мегабайт");
                    driveInfo.Items.Insert(i++, " ");
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ShowDisksInfo();
        
        }
        Thread thread2;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [Obsolete]
        public void MonitorDirAndFile(string path)
        {
            using (FileSystemWatcher watcher = new FileSystemWatcher())
            {
                watcher.Path = path;

                watcher.NotifyFilter = NotifyFilters.LastAccess
                                     | NotifyFilters.LastWrite
                                     | NotifyFilters.FileName
                                     | NotifyFilters.DirectoryName;

                watcher.Filter = "";

                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnRenamed;

                watcher.EnableRaisingEvents = true;

                watcher.WaitForChanged(WatcherChangeTypes.All);

                thread2.Suspend();

                thread2.Start();
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                thread2 = new Thread(() => MonitorDirAndFile(folderBrowserDialog.SelectedPath));
                thread2.Start();
            }
        }
        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            string path = @"D:\Uni\labs\SP\Lab03";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"File: {e.OldFullPath} renamed to {e.FullPath}");
            }
            driveInfo.Items.Clear();
            currentDirectoryInfo.Items.Clear();
            systemDirectoryInfo.Items.Clear();
            ShowDisksInfo();
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            string path = $@"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}/log.txt";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"Файл: {e.FullPath} {e.ChangeType}");
            }
            driveInfo.Items.Clear();
            currentDirectoryInfo.Items.Clear();
            systemDirectoryInfo.Items.Clear();
            ShowDisksInfo();
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            try
            {
                thread2.Resume();
                thread2.Abort();
            }
            catch
            {}
        }
    }
}

